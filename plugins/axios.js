import Vue from "vue";
import Notifications from "~/components/argon-core/NotificationPlugin";
Vue.use(Notifications);
export default function({ $axios, redirect }) {
  $axios.onError((error) => {
    if(!error.response){
      Vue.prototype.$notify({
        message: "Maaf, Sedang Terjadi Masalah",
        timeout: 5000,
        icon: "ni ni-bell-55",
        type: "danger",
      });
      console.log(error);

    }
    if (error.response.status === 401) {
      // console.log(error.response.data.detail);
      // alert("gagal");
      Vue.prototype.$notify({
        message: "Sesi Telah Berakhir",
        timeout: 5000,
        icon: "ni ni-bell-55",
        type: "danger",
      });
      redirect("/");
    }
    if (error.response.status === 500) {
      Vue.prototype.$notify({
        message: "Maaf, Sedang Terjadi Masalah",
        timeout: 5000,
        icon: "ni ni-bell-55",
        type: "danger",
      });
    }
    if (error.response.status === 400 || error.response.status === 404) {
      Vue.prototype.$notify({
        message: error.response.data.detail,
        timeout: 5000,
        icon: "ni ni-bell-55",
        type: "danger",
      });
    }
  });
}
