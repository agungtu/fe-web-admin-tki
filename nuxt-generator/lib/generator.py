import imp
from fastapi import APIRouter, Depends, HTTPException, Response, File, UploadFile, Security
from datetime import datetime
from bson.objectid import ObjectId
from passlib.hash import pbkdf2_sha256
import json
from bson import json_util
from typing import List

def generateForm(label: str, tipe: str, props: str, validation: List[str], config: dict):
    result = ""
    valid = ""
    if validation:
        valid = 'rules='+ "|".join(validation)
    if tipe == "text":
        result = '<base-input\n\
                    name="'+props+'"\n\
                    placeholder="'+label+'"\n\
                    success-message="Looks good!" \n\
                    '+valid+ ' \n\
                    v-model="form.'+props+'"\n\
                ></base-input>'
    else: 
        result = '<base-input\n\
                    name="'+props+'"\n\
                    placeholder="'+label+'"\n\
                    success-message="Looks good!" \n\
                    '+valid+ ' \n\
                    v-model="form.'+props+'"\n\
                ></base-input>'
    return result

def generateTablePerPage():
    result = '<el-select\n\
                class="select-primary pagination-select"\n\
                v-model="paging.size"\n\
                placeholder="Per page"\n\
                @change="changePageSize()"\n\
              >\n\
                <el-option\n\
                  class="select-primary"\n\
                  v-for="item in paging.perPageOptions"\n\
                  :key="item"\n\
                  :label="item"\n\
                  :value="item"\n\
                ></el-option>\n\
              </el-select>'
    return result

def generateTableSeacrh():
    result = '<base-input v-model="searchQuery"\n\
                            prepend-icon="fas fa-search"\n\
                            placeholder="Search...">\n\
                </base-input>'
    return result

def generateTableColumn(label: str, type: str, props: str, width: dict):
    return ""

def generateTableColumnAction(module):
    result = '<el-table-column min-width="180px" align="right" label="Actions">\n\
                <div slot-scope="{$index, row}" class="d-flex">\n\
                  <nuxt-link :to="{name: \''+module.lower()+'-form\', params: {id: row.id_}}">\n\
                    <button class="btn btn-warning btn-sm" type="button">\n\
                      <i class="text-white ni ni-ruler-pencil"></i> Edit\n\
                    </button>\n\
                  </nuxt-link>&nbsp;&nbsp;\n\
                  <base-button\n\
                    @click.native="handleDelete($index, row)"\n\
                    class="remove btn-link"\n\
                    type="danger"\n\
                    size="sm"\n\
                    icon\n\
                  >\n\
                    <i class="text-white ni ni-fat-remove"></i> Delete\n\
                  </base-button>\n\
                </div>\n\
              </el-table-column>'
    return str(result)

def generateTablePaging():
    result = '<div\n\
            slot="footer"\n\
            class="col-12 d-flex justify-content-center justify-content-sm-between flex-wrap"\n\
          >\n\
            <div class>\n\
              <p class="card-category">\n\
                Showing {{ from + 1 }} to {{ to }} of {{ paging.totalElements }} entries\n\
                \n\
              </p>\n\
            </div>\n\
            <ul class="pagination pagination-no-border">\n\
              <li class="page-item prev-page" :class="{disabled: (paging.page + 1) === 1}">\n\
                <a class="page-link" aria-label="Previous" @click="prevPage">\n\
                  <span aria-hidden="true">\n\
                    <i class="fa fa-angle-left" aria-hidden="true"></i>\n\
                  </span>\n\
                </a>\n\
              </li>\n\
              <li\n\
                class="page-item"\n\
                :class="{active: (paging.page + 1) === item}"\n\
                :key="item"\n\
                v-for="item in range(1, paging.totalPages)"\n\
              >\n\
                <a class="page-link" @click="changePage(item)">{{item}}</a>\n\
              </li>\n\
              <li\n\
                class="page-item next-page"\n\
                :class="{disabled: (paging.page + 1) === paging.totalPages}"\n\
              >\n\
                <a class="page-link" aria-label="Next" @click="nextPage">\n\
                  <span aria-hidden="true">\n\
                    <i class="fa fa-angle-right" aria-hidden="true"></i>\n\
                  </span>\n\
                </a>\n\
              </li>\n\
            </ul>\n\
          </div>'
    return str(result)

def generateButton():
    return ""

def generateScriptComponentsIndex():
    result = '\ncomponents: {\n\
                // BasePagination,\n\
                RouteBreadCrumb,\n\
                [Select.name]: Select,\n\
                [Option.name]: Option,\n\
                [Table.name]: Table,\n\
                [TableColumn.name]: TableColumn,\n\
            },'
    return result

def generateScriptDataIndex(urlGetAllData, data):
    columns = "["
    for i in data:
        columns += "{"
        columns += "prop: '" + str(i["props"]) + "',"
        columns += "label: '" + str(i["columnName"]) + "',"
        columns += "minWidth:" + str(i["width"]) + ","
        columns += "sortable: true"
        columns += "}"
    columns += "]"
    result = '\ndata() {\n\
                return {\n\
                tableColumns: '
    result += str(columns)
    result += ',\n\
                tableData: [],\n\
                url: "'
    result += urlGetAllData
    result += '", \n\
                    paging: {\n\
                    size: 10,\n\
                    totalElements: 0,\n\
                    totalPages: 0,\n\
                    page: 0,\n\
                    perPageOptions: [5, 10, 25, 50],\n\
                },\n\
                };\n\
            },'
    return result

def generateScriptBeforeMountIndex():
    result = '\nbeforeMount() {\n\
                this.getDatas(this.paging.size, this.paging.page);\n\
            },'
    return result

def generateScriptComputedIndex():
    result = 'computed: {\n\
                to() {\n\
                let highBound = this.from + this.paging.size;\n\
                if (this.paging.totalElements < highBound) {\n\
                    highBound = this.paging.totalElements;\n\
                }\n\
                return highBound;\n\
                },\n\
                from() {\n\
                return this.paging.size * this.paging.page;\n\
                },\n\
            },'
    return result

def generateScriptMethodsIndex(urlDelete):
    result = '\nmethods: {\n\
    getDatas(size, page) {\n\
      this.$axios\n\
        .$post(this.url + "?size=" + size + "&page=" + page, { search: [] })\n\
        .then((response) => {\n\
          this.tableData = response.content;\n\
          this.tableInfo = response;\n\
          this.paging.totalElements = response.totalElements;\n\
          this.paging.totalPages = response.totalPages;\n\
          this.paging.page = page;\n\
          this.paging.size = size;\n\
        });\n\
    },\n\
    handleLike(index, row) {\n\
      swal({\n\
        title: `You liked ${row.name}`,\n\
        buttonsStyling: false,\n\
        type: "success",\n\
        confirmButtonClass: "btn btn-success btn-fill",\n\
      });\n\
    },\n\
    handleEdit(index, row) {\n\
      console.log(row);\n\
      swal.fire({\n\
        title: `You want to edit ${row.companyName}`,\n\
        buttonsStyling: false,\n\
        confirmButtonClass: "btn btn-info btn-fill",\n\
      });\n\
    },\n\
    handleDelete(index, row) {\n\
      swal\n\
        .fire({\n\
          title: "Are you sure?",\n\
          text: `You wont be able to revert this!`,\n\
          type: "warning",\n\
          showCancelButton: true,\n\
          confirmButtonClass: "btn btn-success btn-fill",\n\
          cancelButtonClass: "btn btn-danger btn-fill",\n\
          confirmButtonText: "Yes, delete it!",\n\
          buttonsStyling: false,\n\
        })\n\
        .then((result) => {\n\
          if (result.value) {\n\
            this.deleteRow(row);\n\
          }\n\
        });\n\
    },\n\
    deleteRow(row) {\n\
      this.$axios\n\
        .$delete("'
    result += urlDelete 
    result += '"+ row.id_)\n\
        .then((user) => {\n\
          swal.fire({\n\
            title: "Deleted!",\n\
            text: `You deleted ${row.companyName}`,\n\
            type: "success",\n\
            confirmButtonClass: "btn btn-success btn-fill",\n\
            buttonsStyling: false,\n\
          });\n\
        })\n\
        .catch((error) => {\n\
          swal.fire({\n\
            title: "Delete Failed!",\n\
            text: `Failed to delete ${row.companyName}`,\n\
            type: "failed",\n\
            confirmButtonClass: "btn btn-danger btn-fill",\n\
            buttonsStyling: false,\n\
          });\n\
        });\n\
    },\n\
    range(min, max) {\n\
      let arr = [];\n\
      for (let i = min; i <= max; i++) {\n\
        arr.push(i);\n\
      }\n\
      return arr;\n\
    },\n\
    changePage(item) {\n\
      const page = item - 1;\n\
      this.getDatas(this.paging.size, page);\n\
    },\n\
    nextPage() {\n\
      const page = this.paging.page + 1;\n\
      this.getDatas(this.paging.size, page);\n\
    },\n\
    prevPage() {\n\
      const page = this.paging.page - 1;\n\
      this.getDatas(this.paging.size, page);\n\
    },\n\
    changePageSize(e) {\n\
      const page = this.paging.page;\n\
      this.getDatas(this.paging.size, page);\n\
    },\n\
  },'
    return result

def generateScriptComponentsForm():
    result = '\ncomponents: {\n\
                TagsInput,\n\
                RouteBreadCrumb,\n\
                [Select.name]: Select,\n\
                [Option.name]: Option,\n\
            },'
    return result

def getDefaultValue(tipe):
    if tipe == "text":
        return '\'\''
    elif tipe == "number":
        return 0
    elif tipe == "tags" or tipe == "multiple":
        return []
    else:
        return '\'\''

def generateScriptDataOtherForm(data):
    result = ""
    for i in data:
        if i["type"] == "dropdown" or i["type"] == "multiple":
            if i["config"][0:3] == "url":
                result += 'dropdown'+i["props"]+ ': [],'
            else:
                result += 'dropdown'+i["props"]+ ':' +i["config"]+","                    
    return result

def generateScriptDataForm(data):
    forms = ",{"
    for i in data:
        forms += str(i["props"])
        forms += ":"
        forms += getDefaultValue(i["type"])
        forms += ","
    forms += "},"

    result = '\ndata() {\n\
                return {\n\
                form: '
    result += str(forms)
    result += generateScriptDataOtherForm(data)
    result += 'validated : false,'
    result += 'doSubmit : true,'
    result += '}'
    result += '},'
    return result

def generateScriptMethodsOtherForm(data):
    result = ""
    for i in data:
        if i["type"] == "dropdown" or i["type"] == "multiple":
            if i["config"][0:3] == "url":
                url_arr = i["config"].split("=")
                result += 'async getDropdown'+i["props"]+'(){\n\
                    this.$axios\n\
                        .$get('+url_arr[1]+').then((response) => {\n\
                            this.dropdown'+i["props"]+' = response.data;\n\
                            })\n\
                            .catch((e) => {});\n\
                    }'
                # result += 'dropdown'+i["props"]+ ': [],'
                                
    return result

def generateScriptBeforeMountForm(data, urlGetData):
    result = '\nbeforeMount() {\n\
                window.addEventListener("keypress", (e) => {\n\
                    if (e.keyCode == 13) {\n\
                    this.doSubmit == false;\n\
                    e.preventDefault();\n\
                    return false;\n\
                    }\n\
                });'
    result += generateScriptMethodsOtherForm(data)
    result += 'this.$axios\n\
                .$get(`'+urlGetData+'${this.$route.params.id}`)\n\
                .then((response) => {'
    for item in data:
        result += 'this.form.'+item["props"]+' = response.'+item["props"]
    result += ' });},'

    return result

def generateScriptMethodsForm(module, urlAdd, urlPut):
    result = '\nmethods: {\n\
            async firstFormSubmit() {\n\
            if(this.doSubmit){\n\
                if(this.form.id == ""){\n\
                    this.$refs.form.validate().then((success) => {\n\
                        if (!success) {\n\
                        return;\n\
                        }\n\
                        \n\
                        this.$axios\n\
                        .$post(`'
    result += urlAdd 
    result += '`, this.form)\n\
                        .then((response) => {\n\
                            swal.fire({\n\
                            title: `Success`,\n\
                            text: "A few words about this sweet alert ...",\n\
                            buttonsStyling: false,\n\
                            confirmButtonClass: "btn btn-success",\n\
                            type: "success",\n\
                            });\n\
                            this.$router.push("/'+module.lower()+'");\n\
                        })\n\
                        .catch((e) => {});\n\
                    });\n\
                }else{\n\
                    this.$refs.form.validate().then((success) => {\n\
                        if (!success) {\n\
                        return;\n\
                        }\n\
                        \n\
                        this.$axios\n\
                        .$put(`'
    result += urlPut 
    result += '${this.$route.params.id}`, this.form)\n\
                        .then((response) => {\n\
                            swal.fire({\n\
                            title: `Success`,\n\
                            text: "A few words about this sweet alert ...",\n\
                            buttonsStyling: false,\n\
                            confirmButtonClass: "btn btn-success",\n\
                            type: "success",\n\
                            });\n\
                            this.$router.push("/'+module.lower()+'");\n\
                        })\n\
                        .catch((e) => {});\n\
                    });\n\
                }\n\
            }\n\
            },}'
    
    return result
