from bson.objectid import ObjectId
from fastapi import APIRouter, Depends, HTTPException, Security
from fastapi.responses import FileResponse
from typing import List
from datetime import datetime
import logging
import string
from fastapi.encoders import jsonable_encoder
from lib.template import getHeader, getTitleFooter, getTitleHeader
from lib.generator import generateTableColumnAction, generateTablePaging, generateTablePerPage , generateTableSeacrh, generateTableColumn, generateForm, generateButton, generateScriptMethodsIndex, generateScriptBeforeMountIndex, generateScriptComponentsIndex, generateScriptComputedIndex, generateScriptDataIndex, generateScriptMethodsOtherForm, generateScriptBeforeMountForm, generateScriptComponentsForm, generateScriptDataForm, generateScriptDataOtherForm, generateScriptMethodsForm, getDefaultValue
import os
import shutil

router_generator = APIRouter()

@router_generator.post("")
async def create_generator(req: dict):
    module = req["moduleName"]
    pageData = req["moduleName"]
    pageCreate = req["pageCreate"]
    urlGetAllData = req["urlGetAllData"]
    urlGetData = req["urlGetData"]
    urlPostData = req["urlPostData"]
    urlPutData = req["urlPutData"]
    urlDeleteData = req["urlDeleteData"]

    data = req["data"]
    create = req["create"]
    index = ""
    add = ""

    if pageData:
        index = generateIndex(module, urlGetAllData, data, urlDeleteData)
    
    if pageCreate:
        add = generateAdd(module, urlGetData, urlPostData, urlPutData, create)

    # Create Directory
    if not os.path.exists(module.lower()):
        os.makedirs(module.lower())

    if index:
        # print(index)
        # index = "123"
        f = open(module + "/index.vue", "w")
        f.write(index)
        f.close()

    if add:
        f = open(module + "/form.vue", "w")
        f.write(add)
        f.close()

    zip_name = module
    directory_name = module
    
    shutil.make_archive(zip_name, 'zip', directory_name)
    filepath = module + ".zip"
    return FileResponse(filepath)

def generateIndex(module, urlGetAllData, dataComp, urlDeleteData):
    results = '<template><div class="content">'    
    results += getHeader('<nuxt-link to="'+module.lower()+'/form" class="btn btn-neutral btn-sm">New</nuxt-link>')
    results += getTitleHeader(module,module)
    
    results += '<div class="col-12 d-flex justify-content-center justify-content-sm-between flex-wrap">';
    results += generateTablePerPage()
    # results += generateTableSeacrh()
    results += '</div>'
    results += '<el-table :data="tableData" row-key="id" header-row-class-name="thead-light">'
    results += '<el-table-column v-for="column in tableColumns" :key="column.label" v-bind="column"></el-table-column>'
    results += generateTableColumnAction(module)
    results += '</el-table>'
    results += generateTablePaging()

    results += getTitleFooter()
    results += '</div></template>'

    #Script Nuxt JS
    results += '<script>'
    results += 'import { Table, TableColumn, Select, Option } from "element-ui";'
    results += 'import RouteBreadCrumb from "@/components/argon-core/Breadcrumb/RouteBreadcrumb";'
    results += 'import swal from "sweetalert2";'
    results += 'export default{'
    results += generateScriptComponentsIndex()
    results += generateScriptDataIndex(urlGetAllData, dataComp)
    results += generateScriptBeforeMountIndex()
    results += generateScriptComputedIndex()
    results += generateScriptMethodsIndex(urlDeleteData)
    results += '};</script>'
    return results

def generateAdd(module, urlGetData, urlPostData, urlPutData, createComp):
    results = '<template><div class="content">'    
    results += getHeader('')
    results += getTitleHeader("Create" + module,module)    
    results += '<div>'
    results += '<validation-observer ref="form">'
    results += '<form class="needs-validation" @submit.prevent="firstFormSubmit">'
    results += '<div class="col-md-8">'

    for item in createComp:
        results += '<div class="form-group row">'
        results += '<label class="col-md-3 col-form-label form-control-label">'+item["columnName"]+'</label>'
        results += '<div class="col-md-9">'
        results += generateForm(item["columnName"], item["type"], item["props"], item["validation"], item["config"])
        results += '</div>'
        results += '</div>'

    results += '</div>'
    results += '</form>'
    results += '</validation-observer>'
    results += '</div>'
    results += getTitleFooter()
    results += '</div></template>'
    #Script Nuxt JS
    results += '\n<script>'
    results += '\nimport TagsInput from "@/components/argon-core/Inputs/TagsInput";'
    results += '\nimport swal from "sweetalert2";'
    results += '\nimport "sweetalert2/dist/sweetalert2.css";'
    results += '\nimport { Select, Option } from "element-ui";'
    results += '\nexport default{'
    results += generateScriptComponentsForm()
    results += generateScriptDataForm(createComp)
    results += generateScriptBeforeMountForm(createComp, urlGetData)    
    results += generateScriptMethodsForm(module, urlPostData, urlPutData)
    results += '\n};</script>'
    return results

